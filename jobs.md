# Jobs

- [Software Engineer Machine Learning - Meta](https://www.metacareers.com/v2/jobs/320163037169073/)
- [Senior Data Scientist - Gojek](https://www.gojek.io/careers/view/senior-data-scientist?id=3d5110bc-2ca0-49d3-ade7-7d76e1be4472)
- [ML Engineer - PT Nomura Research Institute Indonesia](https://www.jobstreet.co.id/machine-learning-engineer-jobs?jobId=73329466&type=standout)
- [ML Engineer - OLX Indonesia](https://www.jobstreet.co.id/machine-learning-engineer-jobs?jobId=73329466&type=standout)
- [AI & ML Program - PT Newfemme Wanita Indonesia](https://www.jobstreet.co.id/machine-learning-engineer-jobs?jobId=73329466&type=standout) 
