# References

## Web Pages
- [Awesome deep learning](https://github.com/ChristosChristofidis/awesome-deep-learning)
- [Awesome machine learning](https://github.com/josephmisiti/awesome-machine-learning/blob/master/courses.md)
- [Machine learning tutorials](https://github.com/ujjwalkarn/Machine-Learning-Tutorials)
- [Best machine learning courses](https://www.learndatasci.com/best-machine-learning-courses/)
    - [Machine learning with Python](https://www.coursera.org/learn/machine-learning-with-python)
    - [CollumbiaX: Machine Learning](https://www.edx.org/learn/machine-learning/columbia-university-machine-learning)
- [Machine learning crash course - Google developers](https://developers.google.com/machine-learning/crash-course/ml-intro) ⭐⭐⭐
- [Mathematics for ML](https://github.com/dair-ai/Mathematics-for-ML)
- [Machine learning specialization - Coursera](https://github.com/greyhatguy007/Machine-Learning-Specialization-Coursera)
- [Frequent Pattern Mining in Data Mining](https://www.geeksforgeeks.org/frequent-pattern-mining-in-data-mining/)
- [Improving Deep Neural Networks: Hyperparameter Tuning, Regularization and Optimization](https://www.coursera.org/learn/deep-neural-network?irclickid=13d05mTdYxyPWWy2gRxTpT3TUkHw%3AtXoowsCSo0&irgwc=1)
- [IBM: Deep Learning with Tensorflow](https://www.edx.org/learn/deep-learning/ibm-deep-learning-with-tensorflow?source=aw&awc=6798_1609431928_0553f31542679853596156120c44555d)
- Dicoding Bangkit
    - [Mathematics for Machine Learning and Data Science Specialization](https://www.coursera.org/specializations/mathematics-for-machine-learning-and-data-science)
    - [Machine Learning Specialization](https://www.coursera.org/specializations/machine-learning-introduction)
    - [TensorFlow Developer Professional Certificate](https://www.deeplearning.ai/courses/tensorflow-developer-professional-certificate/)
    - [Structuring Machine Learning Project](https://www.coursera.org/learn/machine-learning-projects?specialization=deep-learning)
    - [TensorFlow: Advanced Techniques Specialization](https://www.coursera.org/specializations/tensorflow-advanced-techniques)
    - [TensorFlow: Data and Deployment Specialization](https://www.coursera.org/specializations/tensorflow-data-and-deployment)
- Campus courses
    - Carnegie Mellon University 🎖️2
        - [CMU - Introduction to machine learning 2023 🆓](https://www.cs.cmu.edu/~hchai2/courses/10601/)
        - [CMU - Introduction to machine learning](https://www.cs.cmu.edu/~mgormley/courses/10601/coursework.html)
    - Stanford 🎖️3
        - [Stanford - CS229 Spring 2021 Machine Learning](https://cs229.stanford.edu/syllabus-spring2021.html)
    - Oxford 🎖️5
        - [Machine Learning 2022 2023](https://www.cs.ox.ac.uk/teaching/courses/2022-2023/ml/)
    - Princeton 🎖️13
        - [COS 324 - Introduction to Machine Learning](https://princeton-introml.github.io/)
    - MIT 🎖️1
        - [6.036 Introduction to Machine Learning April 2020 🆓](https://openlearninglibrary.mit.edu/courses/course-v1:MITx+6.036+1T2019/course/)
        - [6.036 Introduction to Machine Learning Fall 2020 - Tamara Broderick](https://tamarabroderick.com/ml.html)
        - [Matrix Calculus for Machine Learning and Beyond](https://ocw.mit.edu/courses/18-s096-matrix-calculus-for-machine-learning-and-beyond-january-iap-2023/pages/lecture-notes/)
    - University of Pennsylvania 🎖️43
        - [CIS 520 Machine Learning - 2022 🆓](https://alliance.seas.upenn.edu/~cis520/dynamic/2022/wiki/index.php?n=Lectures.Lectures)
    - University of California Berkeley (UCB) 🎖️4
        - [CS 189/289A Introduction to Machine Learning 🆓](https://people.eecs.berkeley.edu/~jrs/189/)
        - [Professional Certificate in Machine Learning and Artificial Intelligence](https://em-executive.berkeley.edu/professional-certificate-machine-learning-artificial-intelligence)
    - NUS 🎖️6
        - [CS3244 - Machine Learning 2022/2023 Sem 1](https://knmnyn.github.io/cs3244-2210/schedule/)
        - [Advanced Professional Certificate in Applied Artificial Intelligence and Machine Learning](https://ace.nus.edu.sg/apc/advanced-professional-certificate-in-applied-artificial-intelligence-and-machine-learning/)
        - [Applied Machine Learning](https://ace.nus.edu.sg/course/applied-machine-learning/)
    - University of Cambridge 🎖️7
        - [Machine Learning and Bayesian Inference 2022 2023](https://www.cl.cam.ac.uk/teaching/2223/MLBayInfer/)
        - [Introduction to Probability](https://www.cl.cam.ac.uk/teaching/2223/IntroProb/)
        - [Data Science](https://www.cl.cam.ac.uk/teaching/2223/DataSci/)
        - [Representation Learning on Graphs and Networks 2022 2023](https://www.cl.cam.ac.uk/teaching/2223/L45/)
    - Harvard University 🎖️8
        - [Undergraduate Fundamentals of Machine Learning Textbook 2017 🆓](https://harvard-ml-courses.github.io/cs181-web/static/cs181-textbook.pdf)
            - [Schedule 2024](https://harvard-ml-courses.github.io/cs181-web/schedule)
    - ETH Zurich 🎖️9
        - [Introduction to Machine Learning 2023](https://las.inf.ethz.ch/teaching/introml-s23)
    - EPFL École polytechnique fédérale de Lausanne🎖️10
        - [Machine Learning CS-433 🆓](https://www.epfl.ch/labs/mlo/machine-learning-cs-433/)
    - University of Toronto 🎖️12
        - [Machine Learning](https://learning.cs.toronto.edu/courses.html)
        - [CSC 311 Spring 2020: Introduction to Machine Learning 🆓](https://amfarahmand.github.io/csc311/)
        - [CSC 411: Introduction to Machine Learning](https://www.cs.utoronto.ca/~fidler/teaching/2015/CSC411.html)
        - [CSC412/2506 Winter 2020 Probabilistic Learning and Reasoning](https://probmlcourse.github.io/csc412/admin/syllabus.pdf)
    - Imperial College London 🎖️12
        - [Introduction to Machine Learning - COMP70050 Autumn Term 2022/2023 🆓](https://intro2ml.pages.doc.ic.ac.uk/autumn2022/course.html)

## Books
- [Pattern Recognition and Machine Learning - Christopher Bishop ⭐⭐⭐⭐]()
- [Machine Learning - Tom Mitchell ⭐⭐]()
- [Artificial Intelligence - Russel ⭐⭐]()
- [Machine Learning: A Probabilistic Perspective ⭐⭐](https://probml.github.io/pml-book/)
- [Deep Learning - Goodfellow ⭐⭐ 🆓](https://www.deeplearningbook.org/)
- [Deep Learning Foundations and Concepts - Christopher Bishop 🆓](https://www.bishopbook.com/)
- [Mathematics For Machine Learning ⭐ 🆓](https://github.com/mml-book/mml-book.github.io)
- [An Introduction to Statistical Learning ⭐ 🆓](https://www.statlearning.com/)
- [Machine Learning with Python Cookbook, 2nd Edition]()
- [An Introduction to Computational Learning Theory](https://direct.mit.edu/books/book/2604/An-Introduction-to-Computational-Learning-Theory)
- [Introduction to Machine Learning - Etienne Bernard](https://www.wolfram.com/language/introduction-machine-learning/)
- [Information Theory, Inference, and Learning Algorithms ⭐ 🆓](http://www.inference.org.uk/mackay/itila/book.html)

## Topics 

Topics|Rating|References
---|---|---
Basic Python programming|⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐|ICL, Penn, Stanford, CMU, Pacmann, DL.AI, Hacktiv8, Purwadhika, bisa.ai, binar, rakamin, Greatnusa
Machine learning intro|⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐|EPFL, Utoronto, ICL, Harvard, UCB, MIT, Dicoding, Hacktiv8, itbox, bisa.ai, binar, algorit.ma, DQLab, Greatnusa, devgoogle
Mathematical and statistical notation|⭐|Harvard
Information theory|⭐|Penn
Machine learning paradigms|⭐⭐⭐|NUS, Oxford, CMU
Machine learning lifecycle|⭐|Stanford
Computational learning theory|⭐⭐⭐|UCB, Stanford, algorit.ma
Explainable AI|⭐|NUS
Feature representation|⭐⭐|Penn, MIT
Parametric non parametric representation|⭐|Penn
Training and loss function|⭐⭐⭐⭐|EPFL, Harvard, UCB, devgoogle
Objective function|⭐|Harvard
Validation|⭐⭐⭐|EPFL, NUS, UCB
Cross validation|⭐⭐⭐⭐⭐|ICL, Harvard, Oxford, Dicoding, algorit.ma
Testing|⭐|UCB
Maximum likelihood estimation (MLE)|⭐⭐⭐⭐⭐⭐⭐⭐⭐|EPFL, Utoronto, Harvard, Cambridge, UCB, Penn, Oxford, Columbiax
Maximum a posteriori (MAP)|⭐⭐⭐⭐⭐|Cambridge, UCB, Penn, Oxford, Columbiax
Expectation-maximization (EM)|⭐⭐⭐⭐⭐⭐⭐|EPFL, Utoronto, Harvard, Cambridge, Penn, Stanford, Columbiax
Statistical learning|⭐|Princeton
Correlation, causality, feature importance|⭐|Penn
Factor analysis|⭐|Stanford
Lagrange multipliers|⭐|Stanford
Data science|⭐⭐|binar, rakamin
Data collecting|⭐⭐⭐|Dicoding, Purwadhika, itbox
EDA - Data understanding|⭐⭐⭐⭐⭐|Dicoding, Hacktiv8, Purwadhika, rakamin, DQLab
EDA - Missing value & outlier|⭐⭐⭐⭐⭐⭐|Penn, Dicoding, Hacktiv8, itbox, rakamin, DQLab
EDA - Univariate analysis|⭐⭐⭐|Dicoding, Hacktiv8, Purwadhika
EDA - Multivariate analysis|⭐⭐⭐|Dicoding, Hacktiv8, Purwadhika
EDA - Visualization|⭐⭐⭐⭐⭐|Hacktiv8, Purwadikha, binar, rakamin, DQLab
Multivariate calculus|⭐⭐|Stanford, binar
Feature engineering|⭐⭐⭐⭐⭐⭐⭐⭐⭐|NUS, UCB, Dicoding, Hacktiv8, Purwadhika, itbox, algorit.ma, rakamin, DL.AI, devgoogle
Feature encoding|⭐|devgoogle
Oversampling undersampling|⭐|itbox
Preprocessing|⭐⭐⭐|Dicoding, bisa.ai
Dataset management|⭐⭐|Dicoding, devgoogle
Feature selection|⭐|Dicoding
Overfitting underfitting|⭐⭐⭐⭐⭐⭐|EPFL, UCB, Penn, Dicoding, DL.AI, devgoogle
Optimization|⭐⭐⭐⭐⭐⭐⭐⭐⭐|EPFL, ETHZ, Utoronto, UCB, Penn, Oxford, Dicoding, Purwadhika, DQLab
Hyperparameter tuning|⭐⭐⭐⭐⭐|Cambridge, Penn, Dicoding, Purwadhika, DQLab
Grid search|⭐|Dicoding
Randomized search|⭐|DQLab
Gradient descent|⭐⭐⭐⭐⭐⭐⭐⭐⭐|Harvard, UCB, Penn, MIT, DL.AI, algorit.ma, devgoogle, Princeton
Stochastic gradient descent|⭐|UCB
KL divergence|⭐|Penn
Cross entropy|⭐|Penn
Cost function|⭐⭐|UCB, DL.AI
Imbalanced dataset|⭐⭐|Purwadhika, itbox
Generalization|⭐⭐⭐⭐|EPFL, Oxford, Purwadhika, devgoogle
Performance metrics|⭐⭐⭐⭐⭐⭐⭐⭐⭐|ICL, Cambridge, Purwadhika, DL.AI, dicoding, algorit.ma, itbox, rakamin, devgoogle
Confusion matrix|⭐|devgoogle
Precision recall|⭐|devgoogle
ROC AUC|⭐⭐|UCB, devgoogle
Prediction bias|⭐⭐⭐⭐⭐|Harvard, Stanford, devgoogle, Columbiax
Bias variance|⭐⭐⭐⭐⭐⭐⭐|EPFL, ETHZ, Utoronto, Harvard, NUS, Penn, Stanford
Python software engineering|1|Pacmann
Data science project|⭐|Dicoding
Probability|⭐⭐⭐⭐⭐|Penn, Stanford, Pacmann, DL.AI, algorit.ma
Probabilistic modeling|⭐⭐|ETHZ
Probability - odds|⭐|algorit.ma
Bayes rule|⭐⭐⭐⭐⭐⭐⭐|Cambridge, UCB, Oxford, Pacmann, DL.AI, algorit.ma, Columbiax
Bayesian inference|⭐|Cambridge
Bayes risk|⭐|UCB
Bayesian network|⭐|Cambridge
Random variables|⭐⭐⭐|UCB, Pacmann, DL.AI
Statistics|⭐⭐⭐⭐|Pacmann, algorit.ma, binar, rakamin
Statistics - Central tendency|⭐|algorit.ma
Statistics - Variance covariance|⭐|algorit.ma
Statistics - Standard normal curve|⭐|algorit.ma
Statistics - Central limit theorm|⭐|algorit.ma
Statistics - z-Score calculation & student's T-test|⭐|algorit.ma
Statistics - Intervals|⭐|algorit.ma
Statistics - Inferential, hypothesis testing|⭐|algorit.ma
Linear algebra - Vector, matrix, distance|⭐⭐⭐⭐⭐⭐|Penn, Oxford, Stanford, Pacmann, DL.AI, binar
Linear algebra - Least square|⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐|EPFL, Harvard, UCB, Penn, Oxford, Stanford, Pacmann, DL.AI, algorit.ma, binar, Columbiax
Linear algebra - Eigenvalues Eigenvectors|⭐⭐⭐⭐⭐|UCB, Penn, Oxford, Stanford, DL.AI
Newton Rhapson Method|⭐⭐|UCB, Stanford
Generalized linear model|⭐⭐⭐|NUS, Penn, Stanford
Linear classifier|⭐⭐⭐|Cambridge, UCB, MIT
Supervised learning|⭐⭐⭐⭐⭐⭐⭐⭐⭐|Cambridge, Pacmann, DL.AI, Stanford, Dicoding, Hacktiv8, Purwadhika, binar, rakamin, Greatnusa
Classification|⭐⭐⭐⭐⭐⭐|EPFL, ETHZ, Harvard, UCB
Regression|⭐⭐⭐|Harvard
Semi supervised|⭐⭐|Cambridge, Stanford
Ensemble learning|⭐⭐⭐|Utoronto, UCB, Purwadhika
Model - K-nearest neighbour|⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐|EPFL, Utoronto, ICL, Harvard, NUS, UCB, Penn, MIT, CMU, Pacmann, Dicoding, algorit.ma, Greatnusa, IBM, Columbiax
Model - K-means|⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐|EPFL, Utoronto, Cambridge, Penn, Oxford, Stanford, algorit.ma, DQLab, IBM, Columbiax
Graphical model|⭐|Harvard
Model - Naive bayes|⭐⭐⭐⭐⭐|Utoronto, Harvard, Penn, Oxford, Stanford
Softmax|⭐|Harvard
Model - Decision tree|⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐|Utoronto, ICL, NUS, UCB, Penn, MIT, Stanford, CMU, Pacmann, Dicoding, algorit.ma, IBM, Columbiax
Model - Linear regression|⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐|ETHZ, Utoronto, ICL, Harvard, UCB, Penn, MIT, Oxford, CMU, Pacmann, DL.AI, Stanford, Dicoding, algorit.ma, Greatnusa, devgoogle, IBM, Columbiax, Princeton
Model - Ridge regression|⭐⭐⭐|EPFL, UCB, Columbiax
Model - Lasso regression|⭐|EPFL
Model - Polynomial regression|⭐|DL.AI
Model - Regularization|⭐⭐|UCB, Pacmann, DL.AI
Model - Logistic regression|⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐|EPFL, Utoronto, ICL, Harvard, UCB, Penn, MIT, Oxford, Stanford, CMU, Pacmann, Dicoding, algorit.ma, DL.AI, devgoogle, IBM, Columbiax
Kernel methods|⭐⭐⭐⭐⭐⭐⭐⭐|EPFL, ETHZ, Utoronto, Harvard, UCB, Penn, Oxford, Stanford
Model - SVM classification|⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐|EPFL, Utoronto, Harvard, Cambridge, UCB, Penn, Oxford, Stanford, Pacmann, Dicoding, IBM, Columbiax
Model - SV regression|⭐⭐|Pacmann, Dicoding
Model - Bagging|⭐⭐|Utoronto, Pacmann
Model - Boosting|⭐⭐⭐⭐⭐⭐⭐|Utoronto, Harvard, Penn, Stanford, Pacmann, Dicoding, Columbiax
Model - Xgboost|⭐⭐|UCB, DL.AI, Stanford
Model - Adaboost|⭐|UCB
Model - Random forest|⭐⭐⭐⭐⭐⭐⭐|Harvard, Penn, MIT, Pacmann, DL.AI, Stanford, Dicoding, Columbiax
Laplace smoothing|⭐|Stanford
Dimensionality reduction|⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐|ETHZ, Harvard, Oxford, Stanford, Pacmann, Dicoding, algorit.ma, DQLab, Princeton
Model - PCA|⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐|Utoronto, Harvard, UCB, Penn, Oxford, Stanford, Pacmann, Dicoding, algorit.ma, DQLab, Columbiax
Singular value decomposition (SVD)|⭐⭐|UCB, Penn
Autoencoder|⭐|Penn
Model - Latent Dirichlet Allocation LDA|⭐⭐⭐⭐|Harvard, UCB, Penn, itbox
Model - Independent Component Analysis|⭐|Stanford
Model - NMF|⭐|itbox
Model - Clustering|⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐|ETHZ, Utoronto, Harvard, Penn, MIT, Oxford, Pacmann, DL.AI, Stanford, Dicoding, algorit.ma, Columbiax, Princeton
Model - Hierarchical clustering|⭐⭐|Harvard, DQLab
Weak supervision|⭐|Stanford
Self supervision|⭐⭐|EPFL
Gaussian process model|⭐|Cambridge
Gaussian distribution|⭐⭐⭐⭐|Utoronto, UCB, Penn, Stanford
Model selection|⭐⭐⭐⭐⭐|EPFL, ETHZ, Harvard, UCB, Stanford
Mixture model|⭐|Harvard
Gaussian mixtur modele|⭐⭐⭐⭐⭐⭐|EPFL, ETHZ, Harvard, Penn, Stanford, Columbiax
Gaussian discriminant analysis|⭐⭐|UCB, Stanford
Reinforcement learning|⭐⭐⭐⭐⭐⭐⭐⭐⭐|Utoronto, Harvard, Cambridge, Penn, MIT, Stanford, CMU, DL.AI, Stanford, Princeton
SARSA|⭐⭐|Harvard
Value function approximation|⭐|Stanford
Unsupervised learning|⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐|EPFL, ICL, Cambridge, NUS, UCB, Oxford, DL.AI, Stanford, Dicoding, Hacktiv8, Purwadhika, algorit.ma, binar, rakamin, Greatnusa, Princeton
Markov models|⭐⭐⭐⭐⭐|Harvard, Penn, MIT, Columbiax, Princeton
Markov decision process|⭐⭐|Harvard, Penn
Markov random fields|⭐|Cambridge
Markov chain|⭐|Cambridge
Monte carlo methods|⭐|Cambridge
ML workflow|⭐⭐|Pacmann, Dicoding
ML strategy|⭐|DL.AI
CRISP DM|⭐|Dicoding
ML industrial projects|⭐⭐|Penn, Dicoding
ML project design|⭐⭐⭐|Harvard, Penn, Dicoding
ML API|1|Pacmann
ML deployment|⭐⭐⭐⭐|Pacmann, Dicoding, binar, rakamin
ML OPs|⭐|Dicoding
ML Web service|⭐|Dicoding
ML pipeline|⭐⭐⭐|ICL, NUS, Dicoding
ML data ingestion|⭐|Dicoding
ML AutoML|⭐⭐|Penn, Dicoding
ML for business and analytics|⭐|Purwadhika
Data engineering|⭐|Dicoding
Credit scoring analysis|⭐⭐|Dicoding, itbox 
Recommendation System|⭐⭐⭐⭐⭐⭐⭐⭐|Utoronto, UCB, Penn, MIT, Pacmann, DL.AI, Stanford, Purwadhika, Princeton
Association analysis|⭐|Columbiax
Model - Collaborative filtering|⭐⭐⭐⭐|Pacmann, DL.AI, Stanford, Dicoding
Model - Neural Collaborative filtering|⭐|Dicoding
Anomaly detection|⭐|DL.AI, Stanford 
Matrix Factorization Recommender System|⭐⭐⭐⭐⭐|EPFL, Utoronto, Pacmann, Columbiax, Princeton
Contextual Collaborative Filtering|⭐⭐⭐|Pacmann, Dicoding
Hybrid recommender system|⭐|Pacmann, Dicoding
Bayesian Personalized Ranking|⭐|Pacmann
NN - Neural network|⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐|EPFL, ETHZ, Utoronto, ICL, Harvard, UCB, Penn, MIT, Oxford, Stanford, CMU, Pacmann, Dicoding, DL.AI, Hacktiv8, itbox, algorit.ma, bisa.ai, DQLab, devgoogle, Columbiax
NN - Perceptron|⭐⭐⭐⭐⭐|UCB, Penn, MIT, CMU
NN - architecture|⭐⭐⭐|MIT, algorit.ma, devgoogle
NN - From scratch|⭐⭐|MIT, algorit.ma
NN - Deep learning|⭐⭐⭐⭐⭐⭐⭐⭐⭐|NUS, Stanford, Pacmann, Dicoding, itbox, algorit.ma, bisa.ai, binar, Princeton
NN - Computational Graph and Automatic Differentiation|⭐|Pacmann
NN - Feed Forward and Back Propagation|⭐⭐⭐⭐⭐⭐⭐⭐|EPFL, Harvard, MIT, Oxford, Stanford, Pacmann, algorit.ma, Princeton
NN - Activation function|⭐⭐⭐⭐|Neural network, MIT, Pacmann, algorit.ma, devgoogle
NN - Dropout|⭐|EPFL
NN - Regression & classification|1|Pacmann
NN - Batches and Learning Rate|⭐⭐|devgoogle
NN - Regularization|⭐⭐⭐⭐⭐⭐⭐⭐⭐|EPFL, ETHZ, Harvard, NUS, Penn, Oxford, Stanford, Pacmann, devgoogle
NN - Convolutional neural network|⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐|EPFL, Harvard, UCB, Penn, MIT, Oxford, Stanford, Dicoding, DL.AI, bisa.ai, Princeton
NN - Recurrent neural network|⭐⭐⭐⭐|Harvard, MIT, Oxford, CMU
NN - Transformers|⭐⭐⭐|EPFL, Penn, CMU
NN - Self attention|⭐|Penn
NN - Belief net|⭐|Penn
NN - Bayesian neural networks|⭐|Harvard
Object detection|⭐⭐⭐⭐|DL.AI, Dicoding, itbox
Object classification|⭐|DL.AI, Dicoding
Data augmentation|⭐⭐|EPFL, Dicoding
Image augmentation|⭐|Dicoding
Face detection|⭐⭐|Dicoding, itbox
Voice recognition|⭐⭐|itbox, Greatnusa
Generative AI - Basic|⭐⭐⭐⭐⭐⭐⭐⭐|EPFL, ETHZ, Harvard, UCB, Penn, Stanford, CMU, DL.AI
Discriminative model|⭐⭐|Harvard, UCB
Transfer learning|⭐|Dicoding
Active learning|⭐|Penn
Style transfer|⭐|Dicoding
Keras|⭐⭐|Dicoding, itbox
TensorFlow - Basic|⭐⭐|Dicoding,DL.AI
TensorFlow - Custom model, layer, loss function|⭐⭐|DL.AI, devgoogle
TensorFlow - Computer vision|⭐⭐|DL.AI, Dicoding
TensorFlow - Generative deep learning|⭐⭐|Penn, DL.AI
TensorFlow - Data Services API|⭐⭐|DL.AI, Dicoding
TensorFlow - Hub|⭐⭐|DL.AI, Dicoding
TensorFlow - Tensorboard|⭐⭐|DL.AI, Dicoding
TensorFlow - Serving|⭐|DL.AI, Dicoding
TensorFlow - TFX|⭐|Dicoding
Federated learning|⭐|Dicoding
GCP - Intro|⭐|Dicoding
GCP - GCE|⭐|Dicoding
GCP - Cloud run|⭐|Dicoding
GCP - Cloud run|⭐|Dicoding
GCP - Vertex AI|⭐⭐|Dicoding
ML Product - Requirement Gathering and Work Scoping|⭐⭐|Pacmann, Dicoding
ML Product - Framing ML Solution and ML Debt|⭐⭐|Pacmann, Dicoding
LSTM|⭐|Dicoding
Time series|⭐⭐⭐⭐⭐|UCB, Dicoding, Hacktiv8, Purwadhika, algorit.ma
Time series - Predictive analytics|⭐|Dicoding
Time series - Evaluation metrics|⭐|Dicoding
Time series - LSTM|⭐|Dicoding
RL - Intro|⭐|Dicoding
RL - Policy search|⭐|Dicoding
RL - Deep reinforcement learning|⭐|Dicoding
RL - TF-Agents & Deep Q-Network|⭐⭐⭐|Harvard, Penn, Dicoding
NLP - Intro|⭐⭐⭐⭐⭐|UCB, Pacmann, DL.AI, Purwadhika, itbox
NLP - Tokenization|⭐⭐|Dicoding, Purwadhika
NLP - Word vector|⭐⭐⭐|Pacmann, DL.AI, Purwadhika
NLP - TF-IDF|⭐|Dicoding
NLP - Embedding (word, sentence, document)|⭐⭐⭐⭐⭐|EPFL, Pacmann, DL.AI, Dicoding, devgoogle
NLP - Neural language model|⭐|Pacmann
NLP - Text generation|⭐|Pacmann
NLP - Sentiment analysis|⭐|Dicoding
NLP - IndoNLU|⭐|Dicoding
NLP - Binary text classification|⭐⭐|Dicoding, Greatnusa
NLP - Multiclass text classification|⭐|Dicoding
NLP - Chatbot|⭐|Pacmann
NLP - Large language model|⭐|Penn
Big data machine learning|⭐⭐⭐|Dicoding, Hacktiv8, binar
Evolutionary algorithms|⭐|ICL
Web scraping|⭐|Hacktiv8
Version control|⭐|rakamin
Ethics|⭐⭐|EPFL, NUS
Portfolio building - Linkedin Github|⭐⭐⭐⭐
Capstone project|⭐⭐⭐⭐|NUS, UCB

## Related Courses
Maka kita harus ungguli semua ini:
- [Pacmann - Job Preparation Program Max](https://pacmann.io/job-preparation-program-max/aiml-engineer)  
- [Dicoding - Bangkit Academy 2024 By Google, GoTo, Traveloka -  Machine Learning Learning Path](https://kampusmerdeka.kemdikbud.go.id/program/studi-independen/browse/378f6100-0c93-4564-9d60-27f41e62d320/a61142e7-697d-11ee-aa55-9668c84beb67)
    - [Dicoding - Belajar Machine Learning untuk Pemula](https://www.dicoding.com/academies/184)
    - [Dicoding - Belajar Pengembangan Machine Learning](https://www.dicoding.com/academies/185-belajar-pengembangan-machine-learning#course-syllabus-section)
    - [Dicoding - Belajar Penerapan Data Science](https://www.dicoding.com/academies/590)
    - [Dicoding - Machine Learning Terapan](https://www.dicoding.com/academies/319-machine-learning-terapan#course-syllabus-section)
    - [Dicoding - Machine Learning Operations (MLOPs)](https://www.dicoding.com/academies/443-machine-learning-operations-mlops#course-syllabus-section)
- [Hacktiv8 - Python for Data Science](https://www.hacktiv8.com/intro-to-python-for-data-science)
- [ITbox - Belajar Data Science](https://itbox.id/belajar-data-science/)
- [Purwadhika - Data Science & Machine Learning](https://purwadhika.com/job-connector/data-science-and-machine-learning)
- [Bisa.ai - Deep Learing](https://bisa.ai/course/detail/MjU4/1)
    - [Bisa.ai - Master Class + On Job Training: Advance Data Science](https://bisa.ai/course/detail/NDUw/5)
    - [Bisa.ai - Master Class + On Job Training Computer Vision Batch 3](https://bisa.ai/course/detail/NDMz/5)
- [Algorit.ma - Machine Learning Specialization](https://algorit.ma/machine-learning-specialization/)
- [Binar - Data Science Bootcamp](https://product.binaracademy.com/binar-bootcamp/data-science-bootcamp)
- [Rakamin - Data Science Job Guarantee Bootcamp](https://www.rakamin.com/job-guarantee-bootcamp/data-science)
- [DQLab - Bootcamp Machine Learning and AI for Beginner](https://dqlab.id/bootcamp-machine-learning-and-ai-for-beginner)
- [GreatNusa - Pengenalan Machine Learning untuk Pemula](https://greatnusa.com/product/pengenalan-machine-learning-untuk-pemula/)

Dan mengambil inspirasi maksimal dari semua ini:
- [Udemy - Machine Learning A-Z: AI, Python & R + ChatGPT Prize 2024](https://www.udemy.com/course/machinelearning/)  

## Related Articles
- [Forbes - 10 Great Colleges For Studying Artificial Intelligence](https://www.forbes.com/sites/simtumay/2023/08/29/10-great-colleges-for-studying-artificial-intelligence/?sh=7c45490a69ef)

## Related Videos
- [Introduction to Computational Learning Theory](https://www.youtube.com/watch?v=8hJ9V9-f2J8)
