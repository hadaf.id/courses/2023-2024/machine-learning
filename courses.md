# Course

## Material

Topics|Rating|References
---|---|---
Basic Python programming|⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐|ICL, Penn, Stanford, CMU, Pacmann, DL.AI, Hacktiv8, Purwadhika, bisa.ai, binar, rakamin, Greatnusa
Machine learning intro|⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐|EPFL, Utoronto, ICL, Harvard, UCB, MIT, Dicoding, Hacktiv8, itbox, bisa.ai, binar, algorit.ma, DQLab, Greatnusa, devgoogle
Mathematical and statistical notation|⭐|Harvard
Information theory|⭐|Penn
Machine learning paradigms|⭐⭐⭐|NUS, Oxford, CMU
Machine learning lifecycle|⭐|Stanford
Computational learning theory|⭐⭐⭐|UCB, Stanford, algorit.ma
Explainable AI|⭐|NUS
Feature representation|⭐⭐|Penn, MIT
Parametric non parametric representation|⭐|Penn
Training and loss function|⭐⭐⭐⭐|EPFL, Harvard, UCB, devgoogle
Objective function|⭐|Harvard
Validation|⭐⭐⭐|EPFL, NUS, UCB
Cross validation|⭐⭐⭐⭐⭐|ICL, Harvard, Oxford, Dicoding, algorit.ma
Testing|⭐|UCB
Maximum likelihood estimation (MLE)|⭐⭐⭐⭐⭐⭐⭐⭐⭐|EPFL, Utoronto, Harvard, Cambridge, UCB, Penn, Oxford, Columbiax
Maximum a posteriori (MAP)|⭐⭐⭐⭐⭐|Cambridge, UCB, Penn, Oxford, Columbiax
Expectation-maximization (EM)|⭐⭐⭐⭐⭐⭐⭐|EPFL, Utoronto, Harvard, Cambridge, Penn, Stanford, Columbiax
Statistical learning|⭐|Princeton
Correlation, causality, feature importance|⭐|Penn
Factor analysis|⭐|Stanford
Lagrange multipliers|⭐|Stanford
Data science|⭐⭐|binar, rakamin
Data collecting|⭐⭐⭐|Dicoding, Purwadhika, itbox
EDA - Data understanding|⭐⭐⭐⭐⭐|Dicoding, Hacktiv8, Purwadhika, rakamin, DQLab
EDA - Missing value & outlier|⭐⭐⭐⭐⭐⭐|Penn, Dicoding, Hacktiv8, itbox, rakamin, DQLab
EDA - Univariate analysis|⭐⭐⭐|Dicoding, Hacktiv8, Purwadhika
EDA - Multivariate analysis|⭐⭐⭐|Dicoding, Hacktiv8, Purwadhika
EDA - Visualization|⭐⭐⭐⭐⭐|Hacktiv8, Purwadikha, binar, rakamin, DQLab
Multivariate calculus|⭐⭐|Stanford, binar
Feature engineering|⭐⭐⭐⭐⭐⭐⭐⭐⭐|NUS, UCB, Dicoding, Hacktiv8, Purwadhika, itbox, algorit.ma, rakamin, DL.AI, devgoogle
Feature encoding|⭐|devgoogle
Oversampling undersampling|⭐|itbox
Preprocessing|⭐⭐⭐|Dicoding, bisa.ai
Dataset management|⭐⭐|Dicoding, devgoogle
Feature selection|⭐|Dicoding
Overfitting underfitting|⭐⭐⭐⭐⭐⭐|EPFL, UCB, Penn, Dicoding, DL.AI, devgoogle
Optimization|⭐⭐⭐⭐⭐⭐⭐⭐⭐|EPFL, ETHZ, Utoronto, UCB, Penn, Oxford, Dicoding, Purwadhika, DQLab
Hyperparameter tuning|⭐⭐⭐⭐⭐|Cambridge, Penn, Dicoding, Purwadhika, DQLab
Grid search|⭐|Dicoding
Randomized search|⭐|DQLab
Gradient descent|⭐⭐⭐⭐⭐⭐⭐⭐⭐|Harvard, UCB, Penn, MIT, DL.AI, algorit.ma, devgoogle, Princeton
Stochastic gradient descent|⭐|UCB
KL divergence|⭐|Penn
Cross entropy|⭐|Penn
Cost function|⭐⭐|UCB, DL.AI
Imbalanced dataset|⭐⭐|Purwadhika, itbox
Generalization|⭐⭐⭐⭐|EPFL, Oxford, Purwadhika, devgoogle
Performance metrics|⭐⭐⭐⭐⭐⭐⭐⭐⭐|ICL, Cambridge, Purwadhika, DL.AI, dicoding, algorit.ma, itbox, rakamin, devgoogle
Confusion matrix|⭐|devgoogle
Precision recall|⭐|devgoogle
ROC AUC|⭐⭐|UCB, devgoogle
Prediction bias|⭐⭐⭐⭐⭐|Harvard, Stanford, devgoogle, Columbiax
Bias variance|⭐⭐⭐⭐⭐⭐⭐|EPFL, ETHZ, Utoronto, Harvard, NUS, Penn, Stanford
Python software engineering|1|Pacmann
Data science project|⭐|Dicoding
Probability|⭐⭐⭐⭐⭐|Penn, Stanford, Pacmann, DL.AI, algorit.ma
Probabilistic modeling|⭐⭐|ETHZ
Probability - odds|⭐|algorit.ma
Bayes rule|⭐⭐⭐⭐⭐⭐⭐|Cambridge, UCB, Oxford, Pacmann, DL.AI, algorit.ma, Columbiax
Bayesian inference|⭐|Cambridge
Bayes risk|⭐|UCB
Bayesian network|⭐|Cambridge
Random variables|⭐⭐⭐|UCB, Pacmann, DL.AI
Statistics|⭐⭐⭐⭐|Pacmann, algorit.ma, binar, rakamin
Statistics - Central tendency|⭐|algorit.ma
Statistics - Variance covariance|⭐|algorit.ma
Statistics - Standard normal curve|⭐|algorit.ma
Statistics - Central limit theorm|⭐|algorit.ma
Statistics - z-Score calculation & student's T-test|⭐|algorit.ma
Statistics - Intervals|⭐|algorit.ma
Statistics - Inferential, hypothesis testing|⭐|algorit.ma
Linear algebra - Vector, matrix, distance|⭐⭐⭐⭐⭐⭐|Penn, Oxford, Stanford, Pacmann, DL.AI, binar
Linear algebra - Least square|⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐|EPFL, Harvard, UCB, Penn, Oxford, Stanford, Pacmann, DL.AI, algorit.ma, binar, Columbiax
Linear algebra - Eigenvalues Eigenvectors|⭐⭐⭐⭐⭐|UCB, Penn, Oxford, Stanford, DL.AI
Newton Rhapson Method|⭐⭐|UCB, Stanford
Generalized linear model|⭐⭐⭐|NUS, Penn, Stanford
Linear classifier|⭐⭐⭐|Cambridge, UCB, MIT
Supervised learning|⭐⭐⭐⭐⭐⭐⭐⭐⭐|Cambridge, Pacmann, DL.AI, Stanford, Dicoding, Hacktiv8, Purwadhika, binar, rakamin, Greatnusa
Classification|⭐⭐⭐⭐⭐⭐|EPFL, ETHZ, Harvard, UCB
Regression|⭐⭐⭐|Harvard
Semi supervised|⭐⭐|Cambridge, Stanford
Ensemble learning|⭐⭐⭐|Utoronto, UCB, Purwadhika
Model - K-nearest neighbour|⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐|EPFL, Utoronto, ICL, Harvard, NUS, UCB, Penn, MIT, CMU, Pacmann, Dicoding, algorit.ma, Greatnusa, IBM, Columbiax
Model - K-means|⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐|EPFL, Utoronto, Cambridge, Penn, Oxford, Stanford, algorit.ma, DQLab, IBM, Columbiax
Graphical model|⭐|Harvard
Model - Naive bayes|⭐⭐⭐⭐⭐|Utoronto, Harvard, Penn, Oxford, Stanford
Softmax|⭐|Harvard
Model - Decision tree|⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐|Utoronto, ICL, NUS, UCB, Penn, MIT, Stanford, CMU, Pacmann, Dicoding, algorit.ma, IBM, Columbiax
Model - Linear regression|⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐|ETHZ, Utoronto, ICL, Harvard, UCB, Penn, MIT, Oxford, CMU, Pacmann, DL.AI, Stanford, Dicoding, algorit.ma, Greatnusa, devgoogle, IBM, Columbiax, Princeton
Model - Ridge regression|⭐⭐⭐|EPFL, UCB, Columbiax
Model - Lasso regression|⭐|EPFL
Model - Polynomial regression|⭐|DL.AI
Model - Regularization|⭐⭐|UCB, Pacmann, DL.AI
Model - Logistic regression|⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐|EPFL, Utoronto, ICL, Harvard, UCB, Penn, MIT, Oxford, Stanford, CMU, Pacmann, Dicoding, algorit.ma, DL.AI, devgoogle, IBM, Columbiax
Kernel methods|⭐⭐⭐⭐⭐⭐⭐⭐|EPFL, ETHZ, Utoronto, Harvard, UCB, Penn, Oxford, Stanford
Model - SVM classification|⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐|EPFL, Utoronto, Harvard, Cambridge, UCB, Penn, Oxford, Stanford, Pacmann, Dicoding, IBM, Columbiax
Model - SV regression|⭐⭐|Pacmann, Dicoding
Model - Bagging|⭐⭐|Utoronto, Pacmann
Model - Boosting|⭐⭐⭐⭐⭐⭐⭐|Utoronto, Harvard, Penn, Stanford, Pacmann, Dicoding, Columbiax
Model - Xgboost|⭐⭐|UCB, DL.AI, Stanford
Model - Adaboost|⭐|UCB
Model - Random forest|⭐⭐⭐⭐⭐⭐⭐|Harvard, Penn, MIT, Pacmann, DL.AI, Stanford, Dicoding, Columbiax
Laplace smoothing|⭐|Stanford
Dimensionality reduction|⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐|ETHZ, Harvard, Oxford, Stanford, Pacmann, Dicoding, algorit.ma, DQLab, Princeton
Model - PCA|⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐|Utoronto, Harvard, UCB, Penn, Oxford, Stanford, Pacmann, Dicoding, algorit.ma, DQLab, Columbiax
Singular value decomposition (SVD)|⭐⭐|UCB, Penn
Autoencoder|⭐|Penn
Model - Latent Dirichlet Allocation LDA|⭐⭐⭐⭐|Harvard, UCB, Penn, itbox
Model - Independent Component Analysis|⭐|Stanford
Model - NMF|⭐|itbox
Model - Clustering|⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐|ETHZ, Utoronto, Harvard, Penn, MIT, Oxford, Pacmann, DL.AI, Stanford, Dicoding, algorit.ma, Columbiax, Princeton
Model - Hierarchical clustering|⭐⭐|Harvard, DQLab
Weak supervision|⭐|Stanford
Self supervision|⭐⭐|EPFL
Gaussian process model|⭐|Cambridge
Gaussian distribution|⭐⭐⭐⭐|Utoronto, UCB, Penn, Stanford
Model selection|⭐⭐⭐⭐⭐|EPFL, ETHZ, Harvard, UCB, Stanford
Mixture model|⭐|Harvard
Gaussian mixtur modele|⭐⭐⭐⭐⭐⭐|EPFL, ETHZ, Harvard, Penn, Stanford, Columbiax
Gaussian discriminant analysis|⭐⭐|UCB, Stanford
Reinforcement learning|⭐⭐⭐⭐⭐⭐⭐⭐⭐|Utoronto, Harvard, Cambridge, Penn, MIT, Stanford, CMU, DL.AI, Stanford, Princeton
SARSA|⭐⭐|Harvard
Value function approximation|⭐|Stanford
Unsupervised learning|⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐|EPFL, ICL, Cambridge, NUS, UCB, Oxford, DL.AI, Stanford, Dicoding, Hacktiv8, Purwadhika, algorit.ma, binar, rakamin, Greatnusa, Princeton
Markov models|⭐⭐⭐⭐⭐|Harvard, Penn, MIT, Columbiax, Princeton
Markov decision process|⭐⭐|Harvard, Penn
Markov random fields|⭐|Cambridge
Markov chain|⭐|Cambridge
Monte carlo methods|⭐|Cambridge
ML workflow|⭐⭐|Pacmann, Dicoding
ML strategy|⭐|DL.AI
CRISP DM|⭐|Dicoding
ML industrial projects|⭐⭐|Penn, Dicoding
ML project design|⭐⭐⭐|Harvard, Penn, Dicoding
ML API|1|Pacmann
ML deployment|⭐⭐⭐⭐|Pacmann, Dicoding, binar, rakamin
ML OPs|⭐|Dicoding
ML Web service|⭐|Dicoding
ML pipeline|⭐⭐⭐|ICL, NUS, Dicoding
ML data ingestion|⭐|Dicoding
ML AutoML|⭐⭐|Penn, Dicoding
ML for business and analytics|⭐|Purwadhika
Data engineering|⭐|Dicoding
Credit scoring analysis|⭐⭐|Dicoding, itbox 
Recommendation System|⭐⭐⭐⭐⭐⭐⭐⭐|Utoronto, UCB, Penn, MIT, Pacmann, DL.AI, Stanford, Purwadhika, Princeton
Association analysis|⭐|Columbiax
Model - Collaborative filtering|⭐⭐⭐⭐|Pacmann, DL.AI, Stanford, Dicoding
Model - Neural Collaborative filtering|⭐|Dicoding
Anomaly detection|⭐|DL.AI, Stanford 
Matrix Factorization Recommender System|⭐⭐⭐⭐⭐|EPFL, Utoronto, Pacmann, Columbiax, Princeton
Contextual Collaborative Filtering|⭐⭐⭐|Pacmann, Dicoding
Hybrid recommender system|⭐|Pacmann, Dicoding
Bayesian Personalized Ranking|⭐|Pacmann
NN - Neural network|⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐|EPFL, ETHZ, Utoronto, ICL, Harvard, UCB, Penn, MIT, Oxford, Stanford, CMU, Pacmann, Dicoding, DL.AI, Hacktiv8, itbox, algorit.ma, bisa.ai, DQLab, devgoogle, Columbiax
NN - Perceptron|⭐⭐⭐⭐⭐|UCB, Penn, MIT, CMU
NN - architecture|⭐⭐⭐|MIT, algorit.ma, devgoogle
NN - From scratch|⭐⭐|MIT, algorit.ma
NN - Deep learning|⭐⭐⭐⭐⭐⭐⭐⭐⭐|NUS, Stanford, Pacmann, Dicoding, itbox, algorit.ma, bisa.ai, binar, Princeton
NN - Computational Graph and Automatic Differentiation|⭐|Pacmann
NN - Feed Forward and Back Propagation|⭐⭐⭐⭐⭐⭐⭐⭐|EPFL, Harvard, MIT, Oxford, Stanford, Pacmann, algorit.ma, Princeton
NN - Activation function|⭐⭐⭐⭐|Neural network, MIT, Pacmann, algorit.ma, devgoogle
NN - Dropout|⭐|EPFL
NN - Regression & classification|1|Pacmann
NN - Batches and Learning Rate|⭐⭐|devgoogle
NN - Regularization|⭐⭐⭐⭐⭐⭐⭐⭐⭐|EPFL, ETHZ, Harvard, NUS, Penn, Oxford, Stanford, Pacmann, devgoogle
NN - Convolutional neural network|⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐|EPFL, Harvard, UCB, Penn, MIT, Oxford, Stanford, Dicoding, DL.AI, bisa.ai, Princeton
NN - Recurrent neural network|⭐⭐⭐⭐|Harvard, MIT, Oxford, CMU
NN - Transformers|⭐⭐⭐|EPFL, Penn, CMU
NN - Self attention|⭐|Penn
NN - Belief net|⭐|Penn
NN - Bayesian neural networks|⭐|Harvard
Object detection|⭐⭐⭐⭐|DL.AI, Dicoding, itbox
Object classification|⭐|DL.AI, Dicoding
Data augmentation|⭐⭐|EPFL, Dicoding
Image augmentation|⭐|Dicoding
Face detection|⭐⭐|Dicoding, itbox
Voice recognition|⭐⭐|itbox, Greatnusa
Generative AI - Basic|⭐⭐⭐⭐⭐⭐⭐⭐|EPFL, ETHZ, Harvard, UCB, Penn, Stanford, CMU, DL.AI
Discriminative model|⭐⭐|Harvard, UCB
Transfer learning|⭐|Dicoding
Active learning|⭐|Penn
Style transfer|⭐|Dicoding
Keras|⭐⭐|Dicoding, itbox
TensorFlow - Basic|⭐⭐|Dicoding,DL.AI
TensorFlow - Custom model, layer, loss function|⭐⭐|DL.AI, devgoogle
TensorFlow - Computer vision|⭐⭐|DL.AI, Dicoding
TensorFlow - Generative deep learning|⭐⭐|Penn, DL.AI
TensorFlow - Data Services API|⭐⭐|DL.AI, Dicoding
TensorFlow - Hub|⭐⭐|DL.AI, Dicoding
TensorFlow - Tensorboard|⭐⭐|DL.AI, Dicoding
TensorFlow - Serving|⭐|DL.AI, Dicoding
TensorFlow - TFX|⭐|Dicoding
Federated learning|⭐|Dicoding
GCP - Intro|⭐|Dicoding
GCP - GCE|⭐|Dicoding
GCP - Cloud run|⭐|Dicoding
GCP - Cloud run|⭐|Dicoding
GCP - Vertex AI|⭐⭐|Dicoding
ML Product - Requirement Gathering and Work Scoping|⭐⭐|Pacmann, Dicoding
ML Product - Framing ML Solution and ML Debt|⭐⭐|Pacmann, Dicoding
LSTM|⭐|Dicoding
Time series|⭐⭐⭐⭐⭐|UCB, Dicoding, Hacktiv8, Purwadhika, algorit.ma
Time series - Predictive analytics|⭐|Dicoding
Time series - Evaluation metrics|⭐|Dicoding
Time series - LSTM|⭐|Dicoding
RL - Intro|⭐|Dicoding
RL - Policy search|⭐|Dicoding
RL - Deep reinforcement learning|⭐|Dicoding
RL - TF-Agents & Deep Q-Network|⭐⭐⭐|Harvard, Penn, Dicoding
NLP - Intro|⭐⭐⭐⭐⭐|UCB, Pacmann, DL.AI, Purwadhika, itbox
NLP - Tokenization|⭐⭐|Dicoding, Purwadhika
NLP - Word vector|⭐⭐⭐|Pacmann, DL.AI, Purwadhika
NLP - TF-IDF|⭐|Dicoding
NLP - Embedding (word, sentence, document)|⭐⭐⭐⭐⭐|EPFL, Pacmann, DL.AI, Dicoding, devgoogle
NLP - Neural language model|⭐|Pacmann
NLP - Text generation|⭐|Pacmann
NLP - Sentiment analysis|⭐|Dicoding
NLP - IndoNLU|⭐|Dicoding
NLP - Binary text classification|⭐⭐|Dicoding, Greatnusa
NLP - Multiclass text classification|⭐|Dicoding
NLP - Chatbot|⭐|Pacmann
NLP - Large language model|⭐|Penn
Big data machine learning|⭐⭐⭐|Dicoding, Hacktiv8, binar
Evolutionary algorithms|⭐|ICL
Web scraping|⭐|Hacktiv8
Version control|⭐|rakamin
Ethics|⭐⭐|EPFL, NUS
Portfolio building - Linkedin Github|⭐⭐⭐⭐
Capstone project|⭐⭐⭐⭐|NUS, UCB

## Material Summary
- Basic
    - Main ideas
        - Rules in software development 
        - How human programming works ?
            - Problem definition
            - Proposed solution 
                - High level abstraction
                - Low level abstraction
                    - Flow chart
                    - Algorithm
            - Coding process
                - Get the input
                    - Type
                        - Tabular
                        - Unstructured
                            - Image
                            - Audio
                            - Video
                            - Document
                    - Transformation
                        - Suitable representation
                - Process the input
                - Return the output
        - How humans learn the rule of how the things work ? 
            - Human learn by knowledge that acquired from
                - Knowledge base
                - Past experience
                    - Research
                    - Incidental 
            - Example
                - Automation systems
                - Analyzing pattern from data
            - Not every pattern could be inferred easily by human
                - Some patterns are complex
                - Some patterns take a long time to learn
                - Some patterns change over time
        - What if we make the computer to learn from those complex and ambiguous experiences ?
            - So the computer could infer the pattern / rule of the tasks to write the program
        - Machine learning
            - Definition
            - Advantages over rule based
            - Disadvantages over rule based
            - When to use ?
    - Hello world ML with Python
        - Google colab
        - Scikit-learn for tabular data
            - Preprocessing
            - Train
            - Test
        - Keras for object detection 
            - Preprocessing
            - Train
            - Test
    - Computational learning theory 
        - Learning algorithms
    - Experience representation
        - How data can be represented ?
            - Tabular
            - Unstructured
                - Image
                - Audio
                - Video
                - Document
        - Vector
        - Feature engineering process
    - Paradigms
- Machine learning lifecycle for engineers and researchers
    - Project development
        - Data science
        - Artificial intelligence
    - Business understanding
    - Experiments
        - Scientific reviews
        - Dataset management
        - Feature engineering
        - Exploratory data analysis
        - Model development & optimization
    - Scientific publications
    - DevOps
- Learning & inference algorithms
    - Supervised
        - Neural network
        - Linear regression
        - Logistic regression
        - K-Nearest neighbors
    - Unsupervised
        - K-means
    - Reinforcement
- Technologies
    - Python
    - Scikit-learn
    - Tensorflow
    - etc.
- Supported topics
    - Programming
    - Linear algebra
    - Differential calculus
    - Statistics
    - Probability

## Tools

Tools|Rating|Referensi 
---|---|---
Python|⭐⭐|Pacmann, DL.AI
Google Colab|⭐|DQLab
Pandas|⭐⭐|Dicoding, Hacktiv8
Scikit-learn|⭐⭐⭐|Pacmann, Dicoding, DQLab
Keras|⭐⭐⭐|Dicoding, itbox, DQLab
Tensorflow|⭐⭐⭐|Dicoding, DL.AI, devgoogle
Tensorflow js|⭐|DL.AI
Tensorflow lite|⭐|DL.AI
FastAPI|⭐|Pacmann
Docker|⭐|Pacmann
Spacy|⭐|itbox
Streamlit|⭐⭐|Pacmann, DQLab

## Industries 

Industri|Rating|Referensi 
---|---|---
Fintech|1|Pacmann
Telecommunications|1|Pacmann
Ecommerce|1|Pacmann
Consulting|1|Pacmann
Healthcare|1|Pacmann
Finance & banking|1|Pacmann
Startup|1|Pacmann
Transportation & accomodation|1|Pacmann
Retail|1|Pacmann
Government|1|Pacmann
Media|1|Pacmann
FMCG|1|Pacmann

## Features & Services 

Features & Services|Rating|Referensi 
---|---|---
Career consultation|1|Pacmann
Career preparation webinar|1|Pacmann
LMS|1|Pacmann
Recruitment consultation|1|Pacmann
CV & Linkedin review|1|Pacmann
Student consultation|1|Pacmann
Virtual internship|1|Pacmann
Student group & community|1|Pacmann
Mock-up technical test|1|Pacmann
Whatsapp helpdesk|1|Pacmann
Hackthon|1|Pacmann
Project guideline|1|Pacmann
Student portofolio|⭐|Pacmann, DQLab
Gamification|⭐|Rakamin
Simulasi ujian TensorFlow Developer Certificate|⭐|Dicoding

## Datasets 
