# Machine Learning

## Visi Kelas
- Menghasilkan **Machine Learning Researcher dan Engineer** minimal sekaliber Gojek Tokopedia dengan penuh semangat, tanggung jawab, dan riang gembira

## Lesson Learning Outcome
- **Dapat menjelaskan ide dari konsep dan algoritma machine learning** ⭐⭐⭐⭐⭐
    - Ide utama machine learning ⭐⭐⭐⭐⭐
        - Ga semua rumus / aturan dari apapun di dunia ini mudah dipelajari dan diformulasikan oleh manusia, terutama untuk hal-hal yang biasanya membutuhkan peran manusia 
    - Notasi matematika ⭐
    - Paper exploration ⭐
    - Paper writing ⭐
    - Machine learning applications ⭐⭐⭐
        - Industries
        - Software development
        - Product and content development
    - Learning theory ⭐⭐⭐
    - Machine learning berdasarkan bentuk pengalaman ⭐⭐⭐
        - Supervised
        - Unsupervised
        - Semi supervised
        - Reinforcement
    - Supervised
        - Self supervised
        - Regression ⭐⭐⭐⭐
        - Decision tree ⭐⭐⭐
        - Naive bayes ⭐⭐⭐⭐
    - Unsupervised
        - Clustering
            - K-Means ⭐⭐⭐
            - DBScan ⭐⭐⭐
        - Singular value decomposition
            - Principal component analysis
        - Gradient descent
        - Association rule
            - Apriori
            - FP Growth
        - Word embedding
        - Large language model
    - Semi supervised learning
    - Reinforcement ⭐⭐
    - Artificial neural network
        - How ANN works ⭐⭐⭐⭐⭐
        - Deep learning
            - CNN ⭐⭐
                - YOLO
            - RNN ⭐⭐
            - DBN ⭐⭐
            - GNN
            - GAN ⭐⭐
            - Transformers
    - Ensemble learning
        - Boosting bagging
    - Transfer learning
- **Dapat menjalankan tahapan penelitian berbasis machine learning** ⭐⭐⭐⭐⭐
    - Metodologi ⭐⭐⭐⭐
    - Exploratory data analysis ⭐⭐⭐⭐
    - Data visualization ⭐⭐
    - Data transformation ⭐⭐⭐⭐
    - Modeling
        - Preprocessing ⭐⭐⭐⭐⭐
            - Tabular
            - Text
            - Image
            - Audio
        - Training ⭐⭐⭐
            - Overvitting underfitting
        - Validation ⭐⭐⭐ 
        - Hyperparameter tuning ⭐⭐⭐
        - Performance metric ⭐⭐⭐
            - Confusion matrix ⭐⭐⭐⭐⭐
    - Machine learning repositories
        - Huggingface
        - Kaggle
    - Paper writing ⭐⭐⭐
- **Dapat mengaplikasikan machine learning pada sistem** ⭐⭐⭐⭐⭐
    - Machine learning deployment
        - Server
        - Browser
        - Mobile
        - Microcomputer
    - GPU & TPU training & prediction 
    - AutoML
    - MLOps
    - ML cloud services

## Tools

- Python
- Jupyter Lab / Google Colab
- Pandas
- Seaborn
- Scikit-learn
- TensorFlow
- Keras
- MLxtend
- FastAPI
- Docker

- Google Translate
- Duolingo
- HelloTalk
- Grammarly
- Turnitin

## Sessions
1. Pengantar, dasar machine learning disertai contoh aplikasi dunia nyata 
    - Model
        - Training
        - Inference
    - Label
    - Feature
2. Learning process & Regression 
    - Linear regression
    - Training process
    - Learning loss
    - Gradient descent
    - Learning rate
3. Decision tree 
4. Bayes theory & naive bayes
5. Artificial neural network
6. Clustering
7. Machine learning experiment
8. Final project progress
9. Recommendation engine & association rule
10. Deep learning & Convolutional neural network
11. Machine learning deployment & MLOps
12. Optimasi pemodelan (AutoML, Transfer learning, Ensemble learning, Distributed learning)
13. Recurrent neural network, LSTM, & Transformer
14. Generative Adversarial Network
15. Reinforcement learning
16. Final project

## Final Project: Machine Learning Project With Publication (Skripsi / Paper)

## Catatan Fasilitator
- Setiap ide perlu dilengkapi dengan penjelasan ringkas dan sederhana, ilustrasi kuat, contoh aplikasi dunia nyata, dan contoh implementasi coding
- Jangan dijejali terlalu banyak materi, berikan arah keahlian, kuatkan di konsep fundamental
