# Machine Learning Lab Activities

## Visi Kelas
- Menghasilkan **Machine Learning Researcher dan Engineer** minimal sekaliber Gojek Tokopedia dengan penuh semangat, tanggung jawab, dan riang gembira

## Lesson Learning Outcome
- Python installation, package management, Google Colab & Jupyter Notebook
- Data exploration (ingestion, transformation, visualization) from various data sources with Pandas
- Linear regression : Manual vs Scikit-learn
- Decision tree : Manual vs Scikit-learn
- Naive bayes : Manual vs Scikit-learn
- Machine learning experiment 1
- Machine learning experiment 2
- Paper writing
- Machine learning deployment web service
- Machine learning deployment Docker
